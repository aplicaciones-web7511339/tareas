from django.shortcuts import render

from django.views import generic

# Create your views here.

class IndexView(View):
    template_name = "home/index.html"

    def get(self, request):
        context = {
            
        }
        return render(request, self.template_name, context)

class HouseOfCardsView(View):
    template_name = "home/houseofcards.html"

    def get(self, request):
        context = {
            
        }
        return render(request, self.template_name, context)

class ColeccionesView(View):
    template_name = "home/colecciones.html"

    def get(self, request):
        context = {
            
        }
        return render(request, self.template_name, context)